package com.agiledge.rdaphonemaster.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pateelhs_agile on 30/09/18.
 */

public class IOUtil {

    private static IOUtil instance = null;
    private static final String TAG = "IOUtil";

    private IOUtil(){
    }

    public static IOUtil getInstance(){
        if(instance == null) instance = new IOUtil();
        return instance;
    }

    public boolean isNetworkConnected(Context context){
        boolean isConnected = false;
        try {
            ConnectivityManager connMgr = (ConnectivityManager) context.
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nwInfo = connMgr.getActiveNetworkInfo();
            if(nwInfo != null){
                isConnected = nwInfo.isConnected();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            isConnected = false;
        }
        return isConnected;
    }

    public boolean hasPermission(String perm, Activity activity) {
        return (ContextCompat.checkSelfPermission(activity, perm) ==
                PackageManager.PERMISSION_GRANTED);
    }

    public boolean canTakePicture(Activity activity) {
        return (hasPermission(Manifest.permission.CAMERA, activity) && hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, activity));
    }

    public void compressImage(String originalImagePath, File compressedImageFile) throws IOException {
        File compressedImgFile = compressedImageFile;
        File uncompressedFile = new File(originalImagePath);
        String compressedImageFilePath = compressedImageFile.getAbsolutePath();
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap imageBitmap = BitmapFactory.decodeFile(uncompressedFile.getAbsolutePath(), bmOptions);
        if(imageBitmap != null)
            imageBitmap = rotateImage(uncompressedFile.getAbsolutePath().toString(), imageBitmap);
        //Reduce the image resolution in case it's higher than 512x512
        if (imageBitmap.getHeight() > 512 || imageBitmap.getWidth() > 512) {
            Log.i(TAG, "Image height is: " + imageBitmap.getHeight());
            Log.i(TAG, "Image width is: " + imageBitmap.getWidth());
            imageBitmap = getResizedBitmap(imageBitmap, 512);
            Log.i(TAG, "New image height is: " + imageBitmap.getHeight());
            Log.i(TAG, "New image width is: " + imageBitmap.getWidth());
        } else {
            Log.i(TAG, "Image was not rescaled");
            Log.i(TAG, "Image height is: " + imageBitmap.getHeight());
            Log.i(TAG, "Image width is: " + imageBitmap.getWidth());
        }

        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
        //Check if the selected picture size is larger than 1MB, if yes, compress it until size is less than 1MB
        if (uncompressedFile.length() > 1024 * 1024) {
            Log.i(TAG, "Image size is larger than 1MB");
            //start quality with 100 (essentially no compression)
            int quality = 100;
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteOutputStream);
            //Creates an exact copy of the picture in this specific directory (see method saveByteStreamtoFile)
            saveByteStreamtoFile(byteOutputStream, compressedImageFilePath);
            while (byteOutputStream.size() > 1024 * 1024) {
                //Decrease quality (increase compression)
                byteOutputStream.reset();
                quality -= 30;
                Log.i(TAG, "compressing, current ratio: " + quality);
                imageBitmap = BitmapFactory.decodeFile("/sdcard/pp.jpeg", bmOptions);
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteOutputStream);
            }
            compressedImgFile.delete();
            saveByteStreamtoFile(byteOutputStream, compressedImageFilePath);
        } else {
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteOutputStream);
            saveByteStreamtoFile(byteOutputStream, compressedImageFilePath);
        }
    }

    //This method saves the compressed image from the byteStream to a file that can be used
    //to upload the picture to the server
    private void saveByteStreamtoFile(ByteArrayOutputStream os, String path) {
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(path);
            os.writeTo(fos);
            os.flush();
            fos.flush();
            os.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //This method resizes the image while keeping the same aspect ratio
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private Bitmap rotateImage(String path, Bitmap source) throws IOException {
        ExifInterface ei = new ExifInterface(path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        int angle = 0;
        switch(orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                angle = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                angle = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                angle = 270;
                break;
        }
        Bitmap rotatedBitmap;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        rotatedBitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        return rotatedBitmap;
    }


    public String convertImageToBase64(String path){
        Bitmap imageToConvert = BitmapFactory.decodeFile(path);
        if(imageToConvert != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            imageToConvert.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            return encoded;
        }
        return null;
    }

    public void SaveImage(Bitmap finalBitmap, File file) {

        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  static  String datetime(){
        String strCurrentDate="";

        try {
            //Current date to yyyy-mm-dd hh:mm:ss
            Date currentDate = new Date();
            DateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            strCurrentDate = df.format(currentDate);
            System.out.println("Date is " + strCurrentDate);
        } catch (Exception e) {
            System.out.println("Exception :" + e);
        }return strCurrentDate;
    }
}
