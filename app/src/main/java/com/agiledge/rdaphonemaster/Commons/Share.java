package com.agiledge.rdaphonemaster.Commons;

import android.os.Environment;

import com.agiledge.rdaphonemaster.Model.RunAllHistory;
import com.agiledge.rdaphonemaster.Model.RunAllHistoryModel;
import com.agiledge.rdaphonemaster.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Share {
    public static String BACK_BUTTON = "back_button";
    public static String HOME_BUTTON = "home_button";
    public static String PATH = (Environment.getExternalStorageDirectory().getPath() + File.separator + "Phone Tester");
    public static String RUN_ALL_HISTORY_LIST = "run_all_testing";
    public static String TESTING_HISTORY_LIST = "testing_history_list";
    public static String VOLUME_DOWN = "volume_down";
    public static String VOLUME_UP = "volume_up";
    public static String[] checkTestinglist = new String[]{"Check Sim Card", "Check Wifi", "Check Charging", "Check Display", "Check TouchScreen", "Check Sound", "Check Headphone", "Check GPS", "Check Flash", "Check Camera", "Check Vibrate", "Check Proximity Sensor", "Physical Buttons", "Check Accelerometer", "Check Gyroscope", "Check Magnetometer", "Check Light Sensor", "Run all Tests"};
    public static List<RunAllHistoryModel> finalrunAllHistoryModelArrayList = new ArrayList();
    public static boolean isDeleted = false;
    public static int position;
    public static RunAllHistory runAllHistory = new RunAllHistory();
    public static ArrayList<RunAllHistory> runAllHistoryModelArrayList1 = new ArrayList();
    public static boolean runAllTest = false;
    public static int runAllTestPosition = 0;
    public static int screenHeight;
    public static int screenWidth;
    public static Integer[] test_icons_for_history = new Integer[]{Integer.valueOf(R.drawable.ic_sim), Integer.valueOf(R.drawable.ic_wifi), Integer.valueOf(R.drawable.ic_charging), Integer.valueOf(R.drawable.ic_display), Integer.valueOf(R.drawable.ic_touch_screen), Integer.valueOf(R.drawable.ic_sound), Integer.valueOf(R.drawable.ic_sound), Integer.valueOf(R.drawable.ic_sound), Integer.valueOf(R.drawable.ic_headphone), Integer.valueOf(R.drawable.ic_gps), Integer.valueOf(R.drawable.ic_flash), Integer.valueOf(R.drawable.ic_photo_camera), Integer.valueOf(R.drawable.ic_photo_camera), Integer.valueOf(R.drawable.ic_vibration), Integer.valueOf(R.drawable.ic_proximitiy), Integer.valueOf(R.drawable.ic_physical_button), Integer.valueOf(R.drawable.ic_physical_button), Integer.valueOf(R.drawable.ic_physical_button), Integer.valueOf(R.drawable.ic_physical_button), Integer.valueOf(R.drawable.ic_accelerometer), Integer.valueOf(R.drawable.ic_gyroscope), Integer.valueOf(R.drawable.ic_magnatic), Integer.valueOf(R.drawable.ic_light_sensor)};
    public static List<Integer> test_icons_for_history_array = new ArrayList(Arrays.asList(test_icons_for_history));
    public static String[] test_names_for_history = new String[]{"Sim Card", "Wifi", "Charging", "Display", "TouchScreen", "Record", "Ear Piece", "Speaker", "Headphone", "GPS", "Flash", "Rear Camera", "Front Camera", "Vibrate", "Proximity", "Back Button", "Volume Down Button", "Volume Up Button", "Volume Home Button", "Accelerometer", "Gyroscope", "Magnetometer", "Light Sensor"};
    public static List<String> test_names_for_history_array = new ArrayList(Arrays.asList(test_names_for_history));
    public static String[] test_result_for_history = new String[]{"false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false", "false"};
    public static List<String> test_result_for_history_array = new ArrayList(Arrays.asList(test_result_for_history));
    public static Integer[] testingBigIcon = new Integer[]{Integer.valueOf(R.drawable.ic_big_simcard), Integer.valueOf(R.drawable.ic_big_wifi), Integer.valueOf(R.drawable.ic_big_battery), Integer.valueOf(R.drawable.ic_big_iphone), Integer.valueOf(R.drawable.ic_big_touch_screen), Integer.valueOf(R.drawable.ic_big_microphone), Integer.valueOf(R.drawable.ic_big_headphone), Integer.valueOf(R.drawable.ic_big_gps), Integer.valueOf(R.drawable.ic_big_flash), Integer.valueOf(R.drawable.ic_big_camera), Integer.valueOf(R.drawable.ic_big_vibrate), Integer.valueOf(R.drawable.ic_big_proximity), Integer.valueOf(R.drawable.ic_big_physical_button), Integer.valueOf(R.drawable.ic_big_accelerometer), Integer.valueOf(R.drawable.ic_big_gyroscope), Integer.valueOf(R.drawable.ic_big_magnatic), Integer.valueOf(R.drawable.ic_big_light_sensor), Integer.valueOf(R.drawable.ic_big_run_all)};
    public static String[] testingDescription = new String[]{"Check Your SimCard connect\nshould be automatic detected", "Connect Your Phone to a Trusted Wifi\nNetwork", "Connect to Charging Cable\nshould be automatic detected", "Swipe Screen to Check Display", "Draw to test Touch", "Tap Record button and  hold the device as\nYou would normally when speaking and\nsay few words", "Connect Headphone to device.\ntap to check Connection", "Get Current Location", "Tap to Flash Button\nshould be automatic detected", "Check Camera", "Tap Check Vibrate button \nto test Vibration", "Wave your hand to check if Proximity\nsensor is working or not", "Check all Physical buttons", "Check Accelerometer", "Check Gyroscope", "Check Magnetometer", "Check Light Sensor", "Run all Tests"};
    public static Integer[] testingIcon = new Integer[]{Integer.valueOf(R.drawable.ic_sim), Integer.valueOf(R.drawable.ic_wifi), Integer.valueOf(R.drawable.ic_charging), Integer.valueOf(R.drawable.ic_display), Integer.valueOf(R.drawable.ic_touch_screen), Integer.valueOf(R.drawable.ic_sound), Integer.valueOf(R.drawable.ic_headphone), Integer.valueOf(R.drawable.ic_gps), Integer.valueOf(R.drawable.ic_flash), Integer.valueOf(R.drawable.ic_photo_camera), Integer.valueOf(R.drawable.ic_vibration), Integer.valueOf(R.drawable.ic_proximitiy), Integer.valueOf(R.drawable.ic_physical_button), Integer.valueOf(R.drawable.ic_accelerometer), Integer.valueOf(R.drawable.ic_gyroscope), Integer.valueOf(R.drawable.ic_magnatic), Integer.valueOf(R.drawable.ic_light_sensor), Integer.valueOf(R.drawable.ic_run_all)};
    public static String[] testingList = new String[]{"Sim Card", "Wifi", "Charging", "Display", "TouchScreen", "Sound", "Headphone", "GPS", "Flash", "Camera", "Vibrate", "Proximity", "Physical Buttons", "Accelerometer", "Gyroscope", "Magnetometer", "Light Sensor", "Run all Tests"};
    public static long time;


}
