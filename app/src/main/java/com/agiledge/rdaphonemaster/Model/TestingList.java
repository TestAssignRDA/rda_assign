package com.agiledge.rdaphonemaster.Model;

public class TestingList {
    Integer test_image;
    String test_name;

    public Integer getTest_image() {
        return this.test_image;
    }

    public void setTest_image(Integer test_image) {
        this.test_image = test_image;
    }

    public String getTest_name() {
        return this.test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public String toString() {
        return "TestingList{test_image=" + this.test_image + ", test_name='" + this.test_name + '\'' + '}';
    }
}
