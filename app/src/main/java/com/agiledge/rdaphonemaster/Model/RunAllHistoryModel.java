package com.agiledge.rdaphonemaster.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class RunAllHistoryModel implements Serializable {
    ArrayList<RunAllHistoryModel> allHistoryModels = new ArrayList();
    Integer run_all_test_image;
    String run_all_test_name;
    boolean run_all_test_result;
    long run_all_time;

    public boolean isRun_all_test_result() {
        return this.run_all_test_result;
    }

    public void setRun_all_test_result(boolean run_all_test_result) {
        this.run_all_test_result = run_all_test_result;
    }

    public long getRun_all_time() {
        return this.run_all_time;
    }

    public void setRun_all_time(long run_all_time) {
        this.run_all_time = run_all_time;
    }

    public String getRun_all_test_name() {
        return this.run_all_test_name;
    }

    public void setRun_all_test_name(String run_all_test_name) {
        this.run_all_test_name = run_all_test_name;
    }

    public Integer getRun_all_test_image() {
        return this.run_all_test_image;
    }

    public void setRun_all_test_image(Integer run_all_test_image) {
        this.run_all_test_image = run_all_test_image;
    }

    public String toString() {
        return "RunAllHistoryModel{run_all_test_result=" + this.run_all_test_result + ", run_all_time=" + this.run_all_time + ", run_all_test_name='" + this.run_all_test_name + '\'' + ", run_all_test_image=" + this.run_all_test_image + ", allHistoryModels=" + this.allHistoryModels + '}';
    }
}
