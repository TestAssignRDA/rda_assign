package com.agiledge.rdaphonemaster.Model;

public class TestingHistoryModel {
    String Sound_check_type;
    boolean back_button;
    String camera_facing;
    boolean earpiece;
    boolean flash;
    boolean home_button;
    String physical_test_name;
    boolean record;
    boolean speaker;
    Integer test_image;
    String test_name;
    boolean test_result;
    long time;
    boolean vibrate;
    boolean volume_down;
    boolean volume_up;

    public Integer getTest_image() {
        return this.test_image;
    }

    public void setTest_image(Integer test_image) {
        this.test_image = test_image;
    }

    public String getTest_name() {
        return this.test_name;
    }

    public void setTest_name(String test_name) {
        this.test_name = test_name;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isTest_result() {
        return this.test_result;
    }

    public void setTest_result(boolean test_result) {
        this.test_result = test_result;
    }

    public boolean isVibrate() {
        return this.vibrate;
    }

    public void setVibrate(boolean vibrate) {
        this.vibrate = vibrate;
    }

    public boolean isBack_button() {
        return this.back_button;
    }

    public void setBack_button(boolean back_button) {
        this.back_button = back_button;
    }

    public boolean isHome_button() {
        return this.home_button;
    }

    public void setHome_button(boolean home_button) {
        this.home_button = home_button;
    }

    public boolean isVolume_up() {
        return this.volume_up;
    }

    public void setVolume_up(boolean volume_up) {
        this.volume_up = volume_up;
    }

    public boolean isVolume_down() {
        return this.volume_down;
    }

    public void setVolume_down(boolean volume_down) {
        this.volume_down = volume_down;
    }

    public boolean isFlash() {
        return this.flash;
    }

    public void setFlash(boolean flash) {
        this.flash = flash;
    }

    public String getPhysical_test_name() {
        return this.physical_test_name;
    }

    public void setPhysical_test_name(String physical_test_name) {
        this.physical_test_name = physical_test_name;
    }

    public String getCamera_facing() {
        return this.camera_facing;
    }

    public void setCamera_facing(String camera_facing) {
        this.camera_facing = camera_facing;
    }

    public boolean isRecord() {
        return this.record;
    }

    public void setRecord(boolean record) {
        this.record = record;
    }

    public boolean isEarpiece() {
        return this.earpiece;
    }

    public void setEarpiece(boolean earpiece) {
        this.earpiece = earpiece;
    }

    public boolean isSpeaker() {
        return this.speaker;
    }

    public void setSpeaker(boolean speaker) {
        this.speaker = speaker;
    }

    public String getSound_check_type() {
        return this.Sound_check_type;
    }

    public void setSound_check_type(String sound_check_type) {
        this.Sound_check_type = sound_check_type;
    }

    public String toString() {
        return "TestingHistoryModel{test_result=" + this.test_result + ", vibrate=" + this.vibrate + ", flash=" + this.flash + ", back_button=" + this.back_button + ", home_button=" + this.home_button + ", volume_up=" + this.volume_up + ", volume_down=" + this.volume_down + ", record=" + this.record + ", earpiece=" + this.earpiece + ", speaker=" + this.speaker + ", test_image=" + this.test_image + ", test_name='" + this.test_name + '\'' + ", physical_test_name='" + this.physical_test_name + '\'' + ", camera_facing='" + this.camera_facing + '\'' + ", Sound_check_type='" + this.Sound_check_type + '\'' + ", time=" + this.time + '}';
    }
}
