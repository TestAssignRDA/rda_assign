package com.agiledge.rdaphonemaster.Model;

/**
 * Created by pateelhs_agile on 30/09/18.
 */

public class Status {

    String Bluetooth;
    String Mic;
    String Speaker;
    String Camera;
    String LightSensor;
    String Proximity;
    String Vibration;
    String Sim;
    String VolButton;
    String HeadPhone;
    String Flash;
    String Gravity;
    String Pressure;
    String  Accelarometer;
    String Display;
    String Gyro;
    String Magentic;
    String root;
    String wifi;


    public String getBluetooth() {
        return Bluetooth;
    }

    public void setBluetooth(String bluetooth) {
        Bluetooth = bluetooth;
    }

    public String getMic() {
        return Mic;
    }

    public void setMic(String mic) {
        Mic = mic;
    }

    public String getSpeaker() {
        return Speaker;
    }

    public void setSpeaker(String speaker) {
        Speaker = speaker;
    }

    public String getCamera() {
        return Camera;
    }

    public void setCamera(String camera) {
        Camera = camera;
    }

    public String getLightSensor() {
        return LightSensor;
    }

    public void setLightSensor(String lightSensor) {
        LightSensor = lightSensor;
    }

    public String getProximity() {
        return Proximity;
    }

    public void setProximity(String proximity) {
        Proximity = proximity;
    }

    public String getVibration() {
        return Vibration;
    }

    public void setVibration(String vibration) {
        Vibration = vibration;
    }

    public String getSim() {
        return Sim;
    }

    public void setSim(String sim) {
        Sim = sim;
    }

    public String getVolButton() {
        return VolButton;
    }

    public void setVolButton(String volButton) {
        VolButton = volButton;
    }

    public String getFlash() {
        return Flash;
    }

    public void setFlash(String flash) {
        Flash = flash;
    }

    public String getGravity() {
        return Gravity;
    }

    public void setGravity(String gravity) {
        Gravity = gravity;
    }

    public String getPressure() {
        return Pressure;
    }

    public void setPressure(String pressure) {
        Pressure = pressure;
    }

    public String getHeadPhone() {
        return HeadPhone;
    }

    public void setHeadPhone(String headPhone) {
        HeadPhone = headPhone;
    }

    public String getAccelarometer() {
        return Accelarometer;
    }

    public void setAccelarometer(String accelarometer) {
        Accelarometer = accelarometer;
    }

    public String getDisplay() {
        return Display;
    }

    public void setDisplay(String display) {
        Display = display;
    }

    public String getGyro() {
        return Gyro;
    }

    public void setGyro(String gyro) {
        Gyro = gyro;
    }

    public String getMagentic() {
        return Magentic;
    }

    public void setMagentic(String magentic) {
        Magentic = magentic;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getRoot() {
        return root;
    }

    public String getWifi() {
        return wifi;
    }

    public void setWifi(String wifi) {
        this.wifi = wifi;
    }
}
