package com.agiledge.rdaphonemaster.Model;

import java.io.Serializable;
import java.util.List;

public class RunAllHistory implements Serializable {
    List<RunAllHistoryModel> runAllHistoryModels;
    long time;

    public long getTime() {
        return this.time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public List<RunAllHistoryModel> getRunAllHistoryModels() {
        return this.runAllHistoryModels;
    }

    public void setRunAllHistoryModels(List<RunAllHistoryModel> runAllHistoryModels) {
        this.runAllHistoryModels = runAllHistoryModels;
    }

    public String toString() {
        return "RunAllHistory{runAllHistoryModels=" + this.runAllHistoryModels + ", time=" + this.time + '}';
    }
}
