package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.os.Bundle;

import com.agiledge.rdaphonemaster.Model.Status;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class TouchSensor extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new SingleTouchEventView(this, null));

    }

    private void confirm() {
        new SweetAlertDialog(TouchSensor.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Working?")
                .setCancelText("No!")
                .setConfirmText("Yes!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismiss();
                        Status status = new Status();
                        status.setVolButton("Working");
                        finish();


                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        Status status = new Status();
                        status.setVolButton("Not Working");
                        finish();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        confirm();
    }
}
