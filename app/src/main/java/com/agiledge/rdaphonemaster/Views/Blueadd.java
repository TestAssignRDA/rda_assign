package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.widget.TextView;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;


public class Blueadd extends Activity {
    BluetoothAdapter mBluetoothAdapter;
    TextView r,t;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blueadd);
		
		r=(TextView)findViewById(R.id.textView12);
		t=(TextView)findViewById(R.id.textView41);
		r.setText("name "+getLocalBluetoothName());
		t.setText("address "+getLocalBluetoothAddress());
	}

	public String getLocalBluetoothName(){
	    if(mBluetoothAdapter == null){
	        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    }
	    String name = mBluetoothAdapter.getName();
	    if(name == null){
	        System.out.println("Name is null!");
	    }
	      
	    return name;
	}
	public String getLocalBluetoothAddress(){
	    if(mBluetoothAdapter == null){
	        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    }
	    String address = mBluetoothAdapter.getAddress();
	    if(address == null){
	        System.out.println("Address is null!");
			Status status=new Status();
			status.setBluetooth("NOT WORKING");
	    }
	      
	    return address;
	}
	


}
