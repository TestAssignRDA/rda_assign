package com.agiledge.rdaphonemaster.Views;//package com.example.hardware.Views;
//
///**
// * Created by pateelhs_agile on 30/09/18.
// */
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.graphics.Bitmap;
//import android.graphics.Bitmap.Config;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.graphics.Paint.Cap;
//import android.graphics.Paint.Join;
//import android.graphics.Paint.Style;
//import android.graphics.Path;
//import android.hardware.Camera;
//import android.hardware.Camera.Parameters;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.location.Location;
//import android.location.LocationManager;
//import android.media.AudioManager;
//import android.media.MediaPlayer;
//import android.media.MediaRecorder;
//import android.net.ConnectivityManager;
//import android.net.Uri;
//import android.os.Build.VERSION;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.os.Environment;
//import android.os.Handler;
//import android.os.Vibrator;
//import android.provider.Settings.System;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.support.v4.view.ViewPager.OnPageChangeListener;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AlertDialog.Builder;
//import android.support.v7.app.AppCompatActivity;
//import android.telephony.TelephonyManager;
//import android.util.Log;
//import android.view.Display;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.RelativeLayout.LayoutParams;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
//import com.example.hardware.BuildConfig;
//import com.example.hardware.Commons.Share;
//import com.example.hardware.Commons.TinyDB;
//import com.example.hardware.Model.RunAllHistory;
//import com.example.hardware.Model.RunAllHistoryModel;
//import com.example.hardware.Model.TestingHistoryModel;
//import com.example.hardware.R;
//import com.google.android.exoplayer2.DefaultRenderersFactory;
//import com.google.android.exoplayer2.util.MimeTypes;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Random;
//
//public class RunAllTests extends AppCompatActivity implements SensorEventListener, OnClickListener {
//    private static final int SENSOR_SENSITIVITY = 4;
//    public static boolean FRONT_CAMERA = false;
//    public static ArrayList<RunAllHistoryModel> runAllHistoryModelArrayList = new ArrayList();
//    public static ArrayList<TestingHistoryModel> testingHistoryModels = new ArrayList();
//    String AudioSavePathInDevice = null;
//    String RandomAudioFileName = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//    Random random;
//    RoundCornerProgressBar rc_ear_piece_progress;
//    RoundCornerProgressBar rc_record_progress;
//    RoundCornerProgressBar rc_speaker_progress;
//    TestingHistoryModel testingHistoryModel = new TestingHistoryModel();
//    TinyDB tinydb;
//    private int CAMERA_CODE = 432;
//    private boolean EAR_PHONE = false;
//    private boolean IS_EAR_PIECE = false;
//    private boolean IS_RECORDING = false;
//    private boolean IS_SPEAKER = false;
//    private boolean SPEAKER = false;
//    private int STORAGE_PERMISSION_CODE = 23;
//    private String TAG = "RunAllTests";
//    private RunAllTests activity;
//    private boolean back_pressed = false;
//    private Bitmap bitmap;
//    private Canvas canvas;
//    private Uri capturedImageUri = null;
//    private boolean check_pressed = false;
//    private boolean check_touch = false;
//    private boolean color = false;
//    private int[] color_array = new int[]{R.color.black, R.color.white, R.color.blue, R.color.green, R.color.red};
//    private boolean data_added = false;
//    private boolean dialog_showing = false;
//    private String fileName = null;
//    private boolean isFRONT_CAMERA = false;
//    private ImageView iv_back_check;
//    private ImageView iv_home_check;
//    private ImageView iv_test_icon;
//    private ImageView iv_volume_down_check;
//    private ImageView iv_volume_up_check;
//    private long lastUpdate;
//    private List<String> listPermissionsNeeded = new ArrayList();
//    private LinearLayout ll_check;
//    private boolean ll_check_clicked = false;
//    private LinearLayout ll_main;
//
//    private MediaPlayer mPlayer;
//    private Sensor mProximity;
//    private SensorManager mSensorManager;
//    private boolean mStartRecording = false;
//    private MediaRecorder mediaRecorder;
//    private Paint paint;
//    private Path path2;
//    private int progress_value = 0;
//    private String resultStringHistory;
//    private SensorManager sensorManager;
//    private View sketchSheetView;
//    private boolean support_accelerometer;
//    private boolean support_gyroscope;
//    private boolean support_light;
//    private boolean support_magnatometer;
//    private boolean support_proximity;
//    private TextView tv_check;
//    private TextView tv_test_description;
//
//    public static boolean isChargingConnected(Context context) {
//        int plugged = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("plugged", -1);
//        if (plugged == 1 || plugged == 2) {
//            return true;
//        }
//        return false;
//    }
//
//    private static boolean isAirplaneModeOn(Context context) {
//        return System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
//    }
//
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_testing);
//        this.activity = this;
//
//        Log.e(this.TAG + "--->", "Share.position::" + Share.position);
//        Display display = getWindow().getWindowManager().getDefaultDisplay();
//        Share.screenHeight = display.getHeight();
//        Share.screenWidth = display.getWidth();
//        setToolBar();
//        findViews();
//        initViews();
//    }
//
//    protected void onResume() {
//        super.onResume();
//      }
//
//    private void setToolBar() {
//        TextView tv_title = (TextView) findViewById(R.id.tv_title);
//        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
//        ImageView iv_skip = (ImageView) findViewById(R.id.iv_skip);
//        if (Share.runAllTest) {
//            iv_skip.setVisibility(View.VISIBLE);
//
//        } else {
//            iv_skip.setVisibility(View.VISIBLE);
//        }
//        tv_title.setText(Share.testingList[Share.position]);
//        iv_back.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                RunAllTests.this.onBackPressed();
//            }
//        });
//        iv_skip.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                RunAllTests.this.skipTest();
//            }
//        });
//
//    }
//
//    private void findViews() {
//        this.tv_test_description = (TextView) findViewById(R.id.tv_test_description);
//        this.tv_check = (TextView) findViewById(R.id.tv_check);
//        this.iv_test_icon = (ImageView) findViewById(R.id.iv_test_icon);
//        this.ll_check = (LinearLayout) findViewById(R.id.ll_check);
//        this.ll_main = (LinearLayout) findViewById(R.id.ll_main);
//    }
//
//    private void initViews() {
//        this.tinydb = new TinyDB(this.activity);
//        this.sensorManager = (SensorManager) getSystemService("sensor");
//        this.lastUpdate = System.currentTimeMillis();
//        this.tv_test_description.setText(Share.testingDescription[Share.position]);
//        this.tv_check.setText(Share.checkTestinglist[Share.position]);
//        this.iv_test_icon.setImageResource(Share.testingBigIcon[Share.position].intValue());
//        this.mSensorManager = (SensorManager) getSystemService("sensor");
//        this.mProximity = this.mSensorManager.getDefaultSensor(8);
//        this.ll_check.setOnClickListener(this);
//        this.ll_check.setOnTouchListener(new OnTouchListener() {
//            public boolean onTouch(View v, MotionEvent event) {
//                if (Share.position == 10) {
//                    RunAllTests.this.checkVibrator();
//                }
//                return false;
//            }
//        });
//    }
//
//    public void onClick(View v) {
//        if (Share.position == 12) {
//        }
//        if (Share.position == 5) {
//        }
//        if (Share.position == 11) {
//        }
//        if (Share.position == 13) {
//        }
//        if (Share.position == 14) {
//        }
//        if (Share.position == 15) {
//        }
//        if (Share.position != 16) {
//            Log.e(this.TAG + "--->", "onClick if condition");
//            this.testingHistoryModel.setTest_name(Share.testingList[Share.position]);
//            this.testingHistoryModel.setTest_image(Share.testingIcon[Share.position]);
//        }
//        switch (v.getId()) {
//            case R.id.ll_check /*2131296424*/:
//                switch (Share.position) {
//                    case R.styleable.View_android_theme /*0*/:
//                        checkSimCard();
//                        return;
//                    case R.styleable.View_android_focusable /*1*/:
//                        checkWifi();
//                        return;
//                    case R.styleable.View_paddingEnd /*2*/:
//                        if (isChargingConnected(this.activity)) {
//                            resultDialog(getResources().getString(R.string.charging_connected), true);
//                            return;
//                        } else {
//                            resultDialog(getResources().getString(R.string.charging_not_connected), false);
//                            return;
//                        }
//                    case R.styleable.View_paddingStart /*3*/:
//                        checkDisplay();
//                        return;
//                    case SENSOR_SENSITIVITY /*4*/:
//                        this.check_touch = true;
//                        checkTouch();
//                        return;
//                    case R.styleable.Toolbar_contentInsetEnd /*5*/:
//                        checkSound();
//                        return;
//                    case R.styleable.Toolbar_contentInsetEndWithActions /*6*/:
//                        checkHeadphone();
//                        return;
//                    case R.styleable.Toolbar_contentInsetLeft /*7*/:
//                        checkGPS();
//                        return;
//                    case R.styleable.Toolbar_contentInsetRight /*8*/:
//                        this.check_pressed = true;
//                        this.ll_check_clicked = true;
//                        checkFlash();
//                        return;
//                    case R.styleable.Toolbar_contentInsetStart /*9*/:
//                        checkCamera();
//                        return;
//                    case R.styleable.Toolbar_contentInsetStartWithNavigation /*10*/:
//                        this.check_pressed = true;
//                        this.ll_check_clicked = true;
//                        return;
//                    case R.styleable.Toolbar_logo /*11*/:
//                        this.check_pressed = true;
//                        return;
//                    case R.styleable.Toolbar_logoDescription /*12*/:
//                        this.check_pressed = true;
//                        checkPhysicalButton();
//                        return;
//                    case R.styleable.Toolbar_maxButtonHeight /*13*/:
//                        this.check_pressed = true;
//                        if (!this.support_accelerometer) {
//                            resultDialog(getResources().getString(R.string.accelerometer_not_supported), false);
//                            return;
//                        }
//                        return;
//                    case R.styleable.Toolbar_navigationContentDescription /*14*/:
//                        this.check_pressed = true;
//                        if (!this.support_gyroscope) {
//                            Log.e(this.TAG + "-->", "support_gyroscope if");
//                            resultDialog(getResources().getString(R.string.gyroscope_not_supported), false);
//                            return;
//                        }
//                        return;
//                    case R.styleable.Toolbar_navigationIcon /*15*/:
//                        this.check_pressed = true;
//                        if (!this.support_magnatometer) {
//                            resultDialog(getResources().getString(R.string.magnatometer_not_supported), false);
//                            return;
//                        }
//                        return;
//                    case R.styleable.Toolbar_popupTheme /*16*/:
//                        this.check_pressed = true;
//                        if (!this.support_light) {
//                            resultDialog(getResources().getString(R.string.light_not_supported), false);
//                            return;
//                        }
//                        return;
//                    case R.styleable.Toolbar_subtitle /*17*/:
//                        FRONT_CAMERA = false;
//                        runAllTest();
//                        return;
//                    default:
//                        return;
//                }
//            default:
//                return;
//        }
//    }
//
//    public void onSensorChanged(SensorEvent event) {
//        Log.e(this.TAG + "-->", "onSensorChanged called");
//        if (Share.position == 11 && this.check_pressed) {
//            Log.e(this.TAG + BuildConfig.FLAVOR, "support_proximity" + this.support_proximity);
//            if (this.sensorManager.getDefaultSensor(8) == null) {
//                resultDialog(getResources().getString(R.string.proximity_not_supported), false);
//            } else if (event.sensor.getType() == 8 && event.values[0] >= -4.0f && event.values[0] <= 4.0f) {
//                resultDialog(getResources().getString(R.string.prosimity_working), true);
//            }
//        }
//        if (Share.position == 13) {
//            Log.e(this.TAG + "-->", "support_gyroscope 1 ");
//            if (this.check_pressed) {
//                Sensor accelerometerSensor = this.sensorManager.getDefaultSensor(1);
//                if (accelerometerSensor != null) {
//                    Log.e(this.TAG + "-->", "accelerometerSensor in if" + accelerometerSensor);
//                    if (event.sensor.getType() == 1) {
//                        Log.e(this.TAG + "-->", "accelerometerSensor in if inside if" + accelerometerSensor);
//                        getAccelerometer(event);
//                        resultDialog(getResources().getString(R.string.accelerometer_supported), true);
//                    }
//                } else {
//                    Log.e(this.TAG + "-->", "accelerometerSensor in if" + accelerometerSensor);
//                    resultDialog(getResources().getString(R.string.accelerometer_not_supported), false);
//                }
//            }
//        }
//        if (Share.position == 14) {
//            Log.e(this.TAG + "-->", "support_gyroscope ");
//            if (this.check_pressed) {
//                Log.e(this.TAG + "-->", "support_gyroscope else");
//                if (this.sensorManager.getDefaultSensor(SENSOR_SENSITIVITY) != null) {
//                    Log.e(this.TAG + "-->", "support_gyroscope else if");
//                    if (event.sensor.getType() == SENSOR_SENSITIVITY) {
//                        resultDialog(getResources().getString(R.string.gyroscope_supported), true);
//                    } else {
//                        resultDialog(getResources().getString(R.string.gyroscope_not_supported), false);
//                    }
//                } else {
//                    Log.e(this.TAG + "-->", "support_gyroscope else else");
//                    resultDialog(getResources().getString(R.string.gyroscope_not_supported), false);
//                }
//            }
//        }
//        if (Share.position == 15) {
//            Log.e(this.TAG + "-->", "support_gyroscope 3 ");
//            if (this.check_pressed) {
//                Sensor amagnatormeterSensor = this.sensorManager.getDefaultSensor(2);
//                if (amagnatormeterSensor != null) {
//                    Log.e(this.TAG + "-->", "accelerometerSensor in if" + amagnatormeterSensor);
//                    if (event.sensor.getType() == 2) {
//                        Log.e(this.TAG + "-->", "accelerometerSensor in if inside if" + amagnatormeterSensor);
//                        resultDialog(getResources().getString(R.string.magnatometer_supported), true);
//                    }
//                } else {
//                    Log.e(this.TAG + "-->", "accelerometerSensor in if" + amagnatormeterSensor);
//                    resultDialog(getResources().getString(R.string.magnatometer_not_supported), false);
//                }
//            }
//        }
//        if (Share.position == 16 && this.check_pressed) {
//            Sensor lightmeterSensor = this.sensorManager.getDefaultSensor(5);
//            if (lightmeterSensor != null) {
//                Log.e(this.TAG + "-->", "accelerometerSensor in if" + lightmeterSensor);
//                if (event.sensor.getType() == 5) {
//                    Log.e(this.TAG + "-->", "accelerometerSensor in if inside if" + lightmeterSensor);
//                    resultDialog(getResources().getString(R.string.light_supported), true);
//                    return;
//                }
//                return;
//            }
//            Log.e(this.TAG + "-->", "accelerometerSensor in if" + lightmeterSensor);
//            resultDialog(getResources().getString(R.string.light_not_supported), false);
//        }
//    }
//
//    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//    }
//
//    private void getAccelerometer(SensorEvent event) {
//        boolean z = true;
//        float[] values = event.values;
//        float x = values[0];
//        float y = values[1];
//        float z2 = values[2];
//        float accelationSquareRoot = (((x * x) + (y * y)) + (z2 * z2)) / 96.17039f;
//        long actualTime = event.timestamp;
//        if (accelationSquareRoot >= 2.0f) {
//            Log.e(this.TAG + "--->", "getAccelerometer in if");
//            if (actualTime - this.lastUpdate >= 200) {
//                this.lastUpdate = actualTime;
//                if (this.color) {
//                }
//                if (this.color) {
//                    z = false;
//                }
//                this.color = z;
//                return;
//            }
//            return;
//        }
//        Log.e(this.TAG + "--->", "getAccelerometer in else");
//    }
//
//    private void checkSimCard() {
//        switch (((TelephonyManager) getSystemService("phone")).getSimState()) {
//            case R.styleable.View_android_theme /*0*/:
//                Log.e(this.TAG + "--->", "SIM_STATE_UNKNOWN");
//                Share.test_names_for_history_array.add(0, "false");
//                resultDialog(getResources().getString(R.string.simcard_not_available), false);
//                return;
//            case R.styleable.View_android_focusable /*1*/:
//                Log.e(this.TAG + "--->", "SIM_STATE_ABSENT");
//                Share.test_names_for_history_array.add(0, "false");
//                resultDialog(getResources().getString(R.string.simcard_not_available), false);
//                return;
//            case R.styleable.Toolbar_contentInsetEnd /*5*/:
//                Log.e(this.TAG + "--->", "SIM_STATE_READY");
//                Share.test_names_for_history_array.add(0, "true");
//                resultDialog(getResources().getString(R.string.simcard_available), true);
//                return;
//            default:
//                return;
//        }
//    }
//
//    private void checkWifi() {
//        if (((ConnectivityManager) getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
//            Share.test_names_for_history_array.add(1, "true");
//            resultDialog(getResources().getString(R.string.wifi_connected), true);
//            return;
//        }
//        resultDialog(getResources().getString(R.string.wifi_not_connected), false);
//    }
//
//    private void checkDisplay() {
//        View inflatedDisplayLayout = LayoutInflater.from(this.activity).inflate(R.layout.activity_display_check, null, false);
//        this.ll_main.addView(inflatedDisplayLayout);
//        ViewPager vp_main = (ViewPager) inflatedDisplayLayout.findViewById(R.id.vp_main);
//        vp_main.setAdapter(new ColorAdapter(this.activity));
//        vp_main.addOnPageChangeListener(new OnPageChangeListener() {
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            }
//
//            public void onPageSelected(int position) {
//                Log.e(RunAllTests.this.TAG + "--->", "onPageSelected position" + position);
//                if (position == RunAllTests.SENSOR_SENSITIVITY) {
//                    RunAllTests.this.resultDialog(RunAllTests.this.getResources().getString(R.string.display_working), true);
//                }
//            }
//
//            public void onPageScrollStateChanged(int state) {
//            }
//        });
//        vp_main.setOffscreenPageLimit(SENSOR_SENSITIVITY);
//    }
//
//    private void checkTouch() {
//        View old_view = findViewById(R.id.ll_main);
//        ViewGroup parent = (ViewGroup) old_view.getParent();
//        parent.removeView(old_view);
//        View inflatedLayout = getLayoutInflater().inflate(R.layout.activity_touch_check, parent, false);
//        parent.addView(inflatedLayout);
//        RelativeLayout rl_touch = (RelativeLayout) inflatedLayout.findViewById(R.id.rl_touch);
//        this.sketchSheetView = new SketchSheetView(this.activity);
//        this.paint = new Paint();
//        this.path2 = new Path();
//        rl_touch.addView(this.sketchSheetView, new LayoutParams(-1, -1));
//        this.paint.setDither(true);
//        this.paint.setColor(Color.parseColor("#000000"));
//        this.paint.setStyle(Style.STROKE);
//        this.paint.setStrokeJoin(Join.ROUND);
//        this.paint.setStrokeCap(Cap.ROUND);
//        this.paint.setStrokeWidth(2.0f);
//    }
//
//    private void checkSound() {
//        View old_view2 = findViewById(R.id.ll_main);
//        ViewGroup parent2 = (ViewGroup) old_view2.getParent();
//        parent2.removeView(old_view2);
//        View inflatedLayout2 = getLayoutInflater().inflate(R.layout.activity_sound_check, parent2, false);
//        parent2.addView(inflatedLayout2);
//        TextView tv_test_description1 = (TextView) inflatedLayout2.findViewById(R.id.tv_test_description);
//        ImageView iv_test_icon1 = (ImageView) inflatedLayout2.findViewById(R.id.iv_test_icon);
//        final RelativeLayout rl_record = (RelativeLayout) inflatedLayout2.findViewById(R.id.rl_record);
//        RelativeLayout rl_ear_piece = (RelativeLayout) inflatedLayout2.findViewById(R.id.rl_ear_piece);
//        RelativeLayout rl_speaker = (RelativeLayout) inflatedLayout2.findViewById(R.id.rl_speaker);
//        this.rc_record_progress = (RoundCornerProgressBar) inflatedLayout2.findViewById(R.id.rc_record_progress);
//        this.rc_ear_piece_progress = (RoundCornerProgressBar) inflatedLayout2.findViewById(R.id.rc_ear_piece_progress);
//        this.rc_speaker_progress = (RoundCornerProgressBar) inflatedLayout2.findViewById(R.id.rc_speaker_progress);
//        tv_test_description1.setText(Share.testingDescription[Share.position]);
//        iv_test_icon1.setImageResource(Share.testingBigIcon[Share.position].intValue());
//        rl_record.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                Log.e(RunAllTests.this.TAG + "--->", "rl_record called");
//                if (RunAllTests.this.IS_SPEAKER) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.speaker_runnning), 0).show();
//                }
//                if (RunAllTests.this.IS_EAR_PIECE) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.ear_piece_runnning), 0).show();
//                    return;
//                }
//                RunAllTests.this.IS_RECORDING = true;
//                Log.e(RunAllTests.this.TAG + "--->", "rl_record called else");
//                if (RunAllTests.this.mStartRecording) {
//                    rl_record.setEnabled(false);
//                } else {
//                    RunAllTests.this.startRecording();
//                }
//            }
//        });
//        rl_ear_piece.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                if (RunAllTests.this.IS_SPEAKER) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.speaker_runnning), 0).show();
//                }
//                if (RunAllTests.this.IS_RECORDING) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.record_running), 0).show();
//                } else if (RunAllTests.this.fileName == null) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.first_record), 0).show();
//                } else if (!((AudioManager) RunAllTests.this.getSystemService(MimeTypes.BASE_TYPE_AUDIO)).isWiredHeadsetOn()) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.headphone_not_conncted), 0).show();
//                } else if (!RunAllTests.this.EAR_PHONE) {
//                    RunAllTests.this.EAR_PHONE = true;
//                    RunAllTests.this.IS_EAR_PIECE = true;
//                    RunAllTests.this.startPlaying();
//                }
//            }
//        });
//        rl_speaker.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                if (RunAllTests.this.IS_EAR_PIECE) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.ear_piece_runnning), 0).show();
//                }
//                if (RunAllTests.this.IS_RECORDING) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.record_running), 0).show();
//                } else if (((AudioManager) RunAllTests.this.getSystemService(MimeTypes.BASE_TYPE_AUDIO)).isWiredHeadsetOn()) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.headphone_connected), 0).show();
//                } else if (RunAllTests.this.fileName == null) {
//                    Toast.makeText(RunAllTests.this.activity, RunAllTests.this.getResources().getString(R.string.first_record), 0).show();
//                } else if (!RunAllTests.this.SPEAKER) {
//                    RunAllTests.this.SPEAKER = true;
//                    RunAllTests.this.IS_SPEAKER = true;
//                    RunAllTests.this.startPlaying();
//                }
//            }
//        });
//    }
//
//    private void startPlaying() {
//        this.mPlayer = new MediaPlayer();
//        Log.d("instartPlaying", this.fileName);
//        try {
//            this.mPlayer.setDataSource(this.fileName);
//            this.mPlayer.prepare();
//            this.mPlayer.start();
//        } catch (IOException e) {
//            Log.e("LOG_TAG", "prepare() failed");
//        }
//        this.progress_value = 0;
//        final CountDownTimer cdt = new CountDownTimer(6000, 1000) {
//            public void onTick(long millisUntilFinished) {
//                RunAllTests.this.progress_value = RunAllTests.this.progress_value + 1;
//                if (RunAllTests.this.IS_EAR_PIECE) {
//                    RunAllTests.this.rc_ear_piece_progress.setProgress((float) RunAllTests.this.progress_value);
//                } else {
//                    RunAllTests.this.rc_speaker_progress.setProgress((float) RunAllTests.this.progress_value);
//                }
//            }
//
//            public void onFinish() {
//            }
//        }.start();
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                RunAllTests.this.stopPlaying();
//                cdt.cancel();
//                if (RunAllTests.this.IS_EAR_PIECE) {
//                    RunAllTests.this.IS_EAR_PIECE = false;
//                    RunAllTests.this.EAR_PHONE = false;
//                    RunAllTests.this.resultDialog(RunAllTests.this.getResources().getString(R.string.ear_piece_working), true);
//                    return;
//                }
//                RunAllTests.this.IS_SPEAKER = false;
//                RunAllTests.this.SPEAKER = false;
//                RunAllTests.this.resultDialog(RunAllTests.this.getResources().getString(R.string.speaker_working), true);
//            }
//        }, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
//    }
//
//    private void stopPlaying() {
//        try {
//            this.mPlayer.release();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        this.mPlayer = null;
//    }
//
//    private void startRecording() {
//        if (checkAndRequestPermissions()) {
//            Log.e(this.TAG + "--->", "startRecording");
//            this.mediaRecorder = new MediaRecorder();
//            this.mediaRecorder.setAudioSource(1);
//            this.mediaRecorder.setOutputFormat(1);
//            File root = Environment.getExternalStorageDirectory();
//            File file = new File(root.getAbsolutePath() + "/Phone Tester/Audios");
//            if (!file.exists()) {
//                file.mkdirs();
//            }
//            this.fileName = root.getAbsolutePath() + "/Phone Tester/Audios/" + String.valueOf(System.currentTimeMillis() + ".mp3");
//            Log.d("filename", this.fileName);
//            this.mediaRecorder.setOutputFile(this.fileName);
//            this.mediaRecorder.setAudioEncoder(1);
//            if (!this.mStartRecording) {
//                try {
//                    this.mediaRecorder.prepare();
//                    this.mediaRecorder.start();
//                    this.mStartRecording = true;
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            this.progress_value = 0;
//            final CountDownTimer cdt = new CountDownTimer(6000, 1000) {
//                public void onTick(long millisUntilFinished) {
//                    RunAllTests.this.progress_value = RunAllTests.this.progress_value + 1;
//                    RunAllTests.this.rc_record_progress.setProgress((float) RunAllTests.this.progress_value);
//                }
//
//                public void onFinish() {
//                }
//            }.start();
//            new Handler().postDelayed(new Runnable() {
//                public void run() {
//                    Log.e(RunAllTests.this.TAG + "--->", "stopRecording");
//                    cdt.cancel();
//                    RunAllTests.this.resultDialog(RunAllTests.this.getResources().getString(R.string.record_working), true);
//                    RunAllTests.this.IS_RECORDING = false;
//                    RunAllTests.this.stopRecording();
//                }
//            }, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
//            return;
//        }
//        ActivityCompat.requestPermissions(this.activity, (String[]) this.listPermissionsNeeded.toArray(new String[this.listPermissionsNeeded.size()]), this.STORAGE_PERMISSION_CODE);
//    }
//
//    private void stopRecording() {
//        try {
//            this.mediaRecorder.stop();
//            this.mediaRecorder.release();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        this.mediaRecorder = null;
//    }
//
//    public String CreateRandomAudioFileName(int string) {
//        StringBuilder stringBuilder = new StringBuilder(string);
//        for (int i = 0; i < string; i++) {
//            stringBuilder.append(this.RandomAudioFileName.charAt(this.random.nextInt(this.RandomAudioFileName.length())));
//        }
//        return stringBuilder.toString();
//    }
//
//    public void MediaRecorderReady() {
//        this.mediaRecorder = new MediaRecorder();
//        this.mediaRecorder.setAudioSource(1);
//        this.mediaRecorder.setOutputFormat(1);
//        this.mediaRecorder.setAudioEncoder(3);
//        this.mediaRecorder.setOutputFile(this.AudioSavePathInDevice);
//    }
//
//    @SuppressLint("WrongConstant")
//    private void checkHeadphone() {
//        if (((AudioManager) getSystemService(MimeTypes.BASE_TYPE_AUDIO)).isWiredHeadsetOn()) {
//            resultDialog(getResources().getString(R.string.headphone_connected), true);
//        } else {
//            resultDialog(getResources().getString(R.string.headphone_not_conncted), false);
//        }
//    }
//
//    private void checkFlash() {
//        final Camera cam = Camera.open();
//        Parameters p = cam.getParameters();
//        p.setFlashMode("torch");
//        try {
//            cam.setParameters(p);
//            cam.startPreview();
//            if (this.check_pressed) {
//                this.check_pressed = false;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                cam.stopPreview();
//                cam.release();
//            }
//        }, 250);
//    }
//
//    private void checkCamera() {
//        File outPutFile = new File(Share.PATH);
//        if (!outPutFile.exists()) {
//            outPutFile.mkdirs();
//        }
//        try {
//            this.capturedImageUri = Uri.fromFile(File.createTempFile(NewHtcHomeBadger.PACKAGENAME + System.currentTimeMillis(), ".jpg", outPutFile));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//        intent.putExtra("android.intent.extras.CAMERA_FACING", 0);
//        intent.putExtra("output", this.capturedImageUri);
//        startActivityForResult(intent, this.CAMERA_CODE);
//        Log.e(this.TAG + "--->", "FRONT_CAMERA::" + FRONT_CAMERA);
//    }
//
//    private void checkPhysicalButton() {
//        View old_view1 = findViewById(R.id.ll_main);
//        ViewGroup parent1 = (ViewGroup) old_view1.getParent();
//        parent1.removeView(old_view1);
//        View inflatedLayout1 = getLayoutInflater().inflate(R.layout.activity_physical_button_check, parent1, false);
//        parent1.addView(inflatedLayout1);
//        ImageView iv_test_icon = (ImageView) inflatedLayout1.findViewById(R.id.iv_test_icon);
//        ((TextView) inflatedLayout1.findViewById(R.id.tv_test_description)).setText(Share.testingDescription[Share.position]);
//        iv_test_icon.setImageResource(Share.testingBigIcon[Share.position].intValue());
//        this.iv_volume_up_check = (ImageView) inflatedLayout1.findViewById(R.id.iv_volume_up_check);
//        this.iv_volume_down_check = (ImageView) inflatedLayout1.findViewById(R.id.iv_volume_down_check);
//        this.iv_back_check = (ImageView) inflatedLayout1.findViewById(R.id.iv_back_check);
//        this.iv_home_check = (ImageView) inflatedLayout1.findViewById(R.id.iv_home_check);
//    }
//
//    private void checkGPS() {
//        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
//        LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Param.LOCATION);
//        if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0 || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
//            Location net_loc = lm.getLastKnownLocation("network");
//            if (net_loc != null) {
//                resultDialog("Your Current Latitude: " + net_loc.getLatitude() + "\nYour Current Longitude: " + net_loc.getLongitude(), true);
//            } else {
//                resultDialog(getResources().getString(R.string.gps_not_working), false);
//            }
//        }
//    }
//
//    private void checkVibrator() {
//        Vibrator v = (Vibrator) getSystemService("vibrator");
//        if (VERSION.SDK_INT >= 26) {
//            v.vibrate(500);
//        } else {
//            v.vibrate(500);
//        }
//        if (this.check_pressed) {
//            this.check_pressed = false;
//            this.testingHistoryModel.setVibrate(true);
//        }
//    }
//
//    private boolean checkAndRequestPermissions() {
//        this.listPermissionsNeeded.clear();
//        int read_storage = ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE");
//        int storage = ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE");
//        int camera = ContextCompat.checkSelfPermission(this, "android.permission.CAMERA");
//        int record_audio = ContextCompat.checkSelfPermission(this, "android.permission.RECORD_AUDIO");
//        if (read_storage != 0) {
//            this.listPermissionsNeeded.add("android.permission.READ_EXTERNAL_STORAGE");
//        }
//        if (storage != 0) {
//            this.listPermissionsNeeded.add("android.permission.WRITE_EXTERNAL_STORAGE");
//        }
//        if (camera != 0) {
//            this.listPermissionsNeeded.add("android.permission.CAMERA");
//        }
//        if (record_audio != 0) {
//            this.listPermissionsNeeded.add("android.permission.RECORD_AUDIO");
//        }
//        Log.e("TAG", "listPermissionsNeeded===>" + this.listPermissionsNeeded);
//        if (this.listPermissionsNeeded.isEmpty()) {
//            return true;
//        }
//        return false;
//    }
//
//    private void resultDialog(String resultString, final boolean result) {
//        this.resultStringHistory = resultString;
//        if (!Share.runAllTest) {
//            TestingHistoryModel testingHistoryModel;
//            if (Share.position == 11 || Share.position == 13 || Share.position == 14 || Share.position == 15 || Share.position == 16) {
//                if (!this.data_added) {
//                    this.data_added = true;
//                    testingHistoryModel = new TestingHistoryModel();
//                    testingHistoryModel.setTest_name(Share.testingList[Share.position]);
//                    testingHistoryModel.setTest_image(Share.testingIcon[Share.position]);
//                    testingHistoryModel.setTest_result(result);
//                    testingHistoryModel.setTime(System.currentTimeMillis());
//                    testingHistoryModel.setPhysical_test_name("null");
//                    Log.e(this.TAG + "--->", "okclicked resultDialog testingHistoryModel::" + testingHistoryModel);
//                    testingHistoryModels.add(testingHistoryModel);
//                    Log.e(this.TAG + "--->", "testingHistoryModels.add  12");
//                }
//            } else if (Share.position == 5) {
//                testingHistoryModel = new TestingHistoryModel();
//                testingHistoryModel.setTest_name(Share.testingList[Share.position]);
//                testingHistoryModel.setTest_image(Share.testingIcon[Share.position]);
//                Log.e(this.TAG + "--->", "resultString::" + resultString);
//                Log.e(this.TAG + "--->", "R.string.record_working::2131689613");
//                if (resultString.equals(getResources().getString(R.string.record_working))) {
//                    Log.e(this.TAG + "--->", "record_working");
//                    testingHistoryModel.setRecord(true);
//                    testingHistoryModel.setSpeaker(false);
//                    testingHistoryModel.setEarpiece(false);
//                    testingHistoryModel.setSound_check_type("record");
//                }
//                if (resultString.equals(getResources().getString(R.string.ear_piece_working))) {
//                    Log.e(this.TAG + "--->", "ear_piece_working");
//                    testingHistoryModel.setEarpiece(true);
//                    testingHistoryModel.setRecord(false);
//                    testingHistoryModel.setSpeaker(false);
//                    testingHistoryModel.setSound_check_type("ear_piece");
//                }
//                if (resultString.equals(getResources().getString(R.string.speaker_working))) {
//                    Log.e(this.TAG + "--->", "speaker_working");
//                    testingHistoryModel.setSpeaker(true);
//                    testingHistoryModel.setRecord(false);
//                    testingHistoryModel.setEarpiece(false);
//                    testingHistoryModel.setSound_check_type("speaker");
//                }
//                testingHistoryModel.setTime(System.currentTimeMillis());
//                testingHistoryModel.setPhysical_test_name("null");
//                testingHistoryModels.add(testingHistoryModel);
//                Log.e(this.TAG + "--->", "testingHistoryModels.add  10");
//            } else if (!this.dialog_showing) {
//                this.testingHistoryModel.setTest_result(result);
//                this.testingHistoryModel.setTime(System.currentTimeMillis());
//                testingHistoryModels.add(this.testingHistoryModel);
//                Log.e(this.TAG + "--->", "testingHistoryModels.add  11");
//            }
//        }
//        Builder alertDialogBuilder = new Builder(this.activity, R.style.alert_dialog);
//        alertDialogBuilder.setMessage(resultString).setCancelable(false).setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                RunAllTests.this.dialog_showing = false;
//                Log.e(RunAllTests.this.TAG + "--->", "Share.position::" + Share.position + "FRONT_CAMERA::" + RunAllTests.FRONT_CAMERA);
//                if (Share.position != 5) {
//                    RunAllTests.this.finish();
//                }
//                Log.e(RunAllTests.this.TAG + "-->", "Share.position-:" + Share.position);
//                RunAllTests.this.nextTest(result);
//            }
//        });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        if (!this.dialog_showing) {
//            try {
//                if (!this.activity.isFinishing()) {
//                    alertDialog.show();
//                    this.dialog_showing = true;
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    private void runAllTest() {
//        Share.time = System.currentTimeMillis();
//        Share.runAllHistory = new RunAllHistory();
//        runAllHistoryModelArrayList.clear();
//        Share.runAllTest = true;
//        Share.position = 0;
//        callTestActivity();
//    }
//
//    private void skipTest() {
//        Log.e(this.TAG + "-->", "Share.position-:" + Share.position);
//        if (Share.position == 16) {
//            this.tinydb.putListObjectRunALl(Share.RUN_ALL_HISTORY_LIST, runAllHistoryModelArrayList);
//            startActivity(new Intent(this.activity, RunAllTestResultActivity.class));
//            finish();
//        } else if (Share.position == SENSOR_SENSITIVITY) {
//            Log.e(this.TAG + "-->", "skipTest:: in if check_touch" + this.check_touch);
//            if (this.check_touch) {
//                askResultToUser();
//                return;
//            }
//            this.check_touch = false;
//            Share.position++;
//            callTestActivity();
//        } else if (Share.position == 8) {
//            if (this.ll_check_clicked) {
//                askResultToUser1();
//                return;
//            }
//            this.ll_check_clicked = false;
//            Share.position++;
//            callTestActivity();
//        } else if (Share.position != 10) {
//            Share.position++;
//            callTestActivity();
//        } else if (this.ll_check_clicked) {
//            askResultToUser1();
//        } else {
//            this.ll_check_clicked = false;
//            Share.position++;
//            callTestActivity();
//        }
//    }
//
//    private void callTestActivity() {
//        startActivity(new Intent(this.activity, RunAllTests.class));
//        finish();
//    }
//
//    protected void onUserLeaveHint() {
//        super.onUserLeaveHint();
//        if (Share.position == 12 && this.check_pressed) {
//            this.iv_home_check.setVisibility(0);
//            if (Share.runAllTest) {
//                RunAllHistoryModel runAllHistoryModel = new RunAllHistoryModel();
//                runAllHistoryModel.setRun_all_test_name("Home Button");
//                runAllHistoryModel.setRun_all_test_image(Share.testingIcon[Share.position]);
//                runAllHistoryModel.setRun_all_test_result(true);
//                runAllHistoryModel.setRun_all_time(Share.time);
//                Log.e(this.TAG + "===>", "Share.time::" + Share.time);
//                runAllHistoryModelArrayList.add(runAllHistoryModel);
//            } else {
//                TestingHistoryModel testingHistoryModel = new TestingHistoryModel();
//                testingHistoryModel.setTest_name(Share.testingList[Share.position]);
//                testingHistoryModel.setTest_image(Share.testingIcon[Share.position]);
//                testingHistoryModel.setHome_button(true);
//                testingHistoryModel.setTime(System.currentTimeMillis());
//                testingHistoryModel.setPhysical_test_name(Share.HOME_BUTTON);
//                testingHistoryModels.add(testingHistoryModel);
//            }
//            Log.e(this.TAG + "--->", "testingHistoryModels.add  1");
//            Share.sortHistoryArray();
//            this.tinydb.putListObject(Share.TESTING_HISTORY_LIST, testingHistoryModels);
//        }
//    }
//
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        RunAllHistoryModel runAllHistoryModel;
//        TestingHistoryModel testingHistoryModel;
//        if (keyCode == 25 && Share.position == 12) {
//            this.iv_volume_down_check.setVisibility(View.VISIBLE);
//            if (Share.runAllTest) {
//                runAllHistoryModel = new RunAllHistoryModel();
//                runAllHistoryModel.setRun_all_test_name("Volume Down Button");
//                runAllHistoryModel.setRun_all_test_image(Share.testingIcon[Share.position]);
//                runAllHistoryModel.setRun_all_test_result(true);
//                runAllHistoryModel.setRun_all_time(Share.time);
//                Log.e(this.TAG + "===>", "Share.time::" + Share.time);
//                runAllHistoryModelArrayList.add(runAllHistoryModel);
//                return true;
//            }
//            testingHistoryModel = new TestingHistoryModel();
//            testingHistoryModel.setTest_name(Share.testingList[Share.position]);
//            testingHistoryModel.setTest_image(Share.testingIcon[Share.position]);
//            testingHistoryModel.setVolume_down(true);
//            testingHistoryModel.setTime(System.currentTimeMillis());
//            testingHistoryModel.setPhysical_test_name(Share.VOLUME_DOWN);
//            testingHistoryModels.add(testingHistoryModel);
//            return true;
//        } else if (keyCode == 24 && Share.position == 12) {
//            this.iv_volume_up_check.setVisibility(0);
//            if (Share.runAllTest) {
//                runAllHistoryModel = new RunAllHistoryModel();
//                runAllHistoryModel.setRun_all_test_name("Volume Up Button");
//                runAllHistoryModel.setRun_all_test_image(Share.testingIcon[Share.position]);
//                runAllHistoryModel.setRun_all_test_result(true);
//                runAllHistoryModel.setRun_all_time(Share.time);
//                Log.e(this.TAG + "===>", "Share.time::" + Share.time);
//                runAllHistoryModelArrayList.add(runAllHistoryModel);
//                return true;
//            }
//            testingHistoryModel = new TestingHistoryModel();
//            testingHistoryModel.setTest_name(Share.testingList[Share.position]);
//            testingHistoryModel.setTest_image(Share.testingIcon[Share.position]);
//            testingHistoryModel.setVolume_up(true);
//            testingHistoryModel.setTime(System.currentTimeMillis());
//            testingHistoryModel.setPhysical_test_name(Share.VOLUME_UP);
//            testingHistoryModels.add(testingHistoryModel);
//            return true;
//        } else {
//            if (keyCode == 3) {
//                Log.e(this.TAG + "--->", "KEYCODE_HOME");
//                if (Share.position == 12) {
//                    this.iv_home_check.setVisibility(0);
//                    return true;
//                }
//            }
//            return super.onKeyDown(keyCode, event);
//        }
//    }
//
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.e(this.TAG, "resultCode :" + resultCode);
//        if (requestCode == this.STORAGE_PERMISSION_CODE && resultCode == -1) {
//            startRecording();
//        }
//        if (requestCode == this.CAMERA_CODE) {
//            Log.e(this.TAG + "--->", "CAMERA_CODE");
//            if (resultCode == -1) {
//                Log.e(this.TAG + "--->", "RESULT_OK");
//                Log.e(this.TAG + "--->", "capturedImageUri::" + this.capturedImageUri);
//                if (this.capturedImageUri != null) {
//                    File file = new File(this.capturedImageUri.getPath());
//                    Log.e(this.TAG + "--->", "file:::" + file);
//                    if (file.exists()) {
//                        Log.e(this.TAG + "--->", "result success");
//                        if (FRONT_CAMERA) {
//                            this.isFRONT_CAMERA = true;
//                            resultDialog(getResources().getString(R.string.front_camera_working), true);
//                            FRONT_CAMERA = false;
//                            return;
//                        }
//                        this.isFRONT_CAMERA = false;
//                        resultDialog(getResources().getString(R.string.rear_camera_working), true);
//                        FRONT_CAMERA = true;
//                        return;
//                    }
//                    TestingHistoryModel testingHistoryModel = new TestingHistoryModel();
//                    if (FRONT_CAMERA) {
//                        this.isFRONT_CAMERA = true;
//                        resultDialog(getResources().getString(R.string.front_camera_not_working), false);
//                        return;
//                    }
//                    this.isFRONT_CAMERA = false;
//                    resultDialog(getResources().getString(R.string.rear_camera_not_working), false);
//                    FRONT_CAMERA = false;
//                }
//            }
//        }
//    }
//
//    private void addInHistoryList() {
//        TestingHistoryModel testingHistoryModel = new TestingHistoryModel();
//        testingHistoryModel.setTest_name(Share.testingList[17]);
//        testingHistoryModel.setTest_image(Share.testingIcon[17]);
//        testingHistoryModel.setTime(Share.time);
//        testingHistoryModel.setPhysical_test_name("null");
//        Log.e(this.TAG + "--->", "okclicked resultDialog testingHistoryModel::" + testingHistoryModel);
//        testingHistoryModels.add(testingHistoryModel);
//        Share.sortHistoryArray();
//        this.tinydb.putListObject(Share.TESTING_HISTORY_LIST, testingHistoryModels);
//    }
//
//    public void onBackPressed() {
//        if (Share.position == 12) {
//            if (!this.check_pressed) {
//                Log.e(this.TAG + "--->", "onBackPressed 2");
//                this.check_pressed = false;
//                super.onBackPressed();
//            } else if (this.iv_back_check.getVisibility() == 8) {
//                this.iv_back_check.setVisibility(View.VISIBLE);
//                if (Share.runAllTest) {
//                    RunAllHistoryModel runAllHistoryModel = new RunAllHistoryModel();
//                    runAllHistoryModel.setRun_all_test_name("Back Button");
//                    runAllHistoryModel.setRun_all_test_image(Share.testingIcon[Share.position]);
//                    runAllHistoryModel.setRun_all_test_result(true);
//                    runAllHistoryModel.setRun_all_time(Share.time);
//                    Log.e(this.TAG + "===>", "Share.time::" + Share.time);
//                    runAllHistoryModelArrayList.add(runAllHistoryModel);
//                } else {
//                    TestingHistoryModel testingHistoryModel = new TestingHistoryModel();
//                    testingHistoryModel.setTest_name(Share.testingList[Share.position]);
//                    testingHistoryModel.setTest_image(Share.testingIcon[Share.position]);
//                    testingHistoryModel.setBack_button(true);
//                    testingHistoryModel.setTime(System.currentTimeMillis());
//                    testingHistoryModel.setPhysical_test_name(Share.BACK_BUTTON);
//                    testingHistoryModels.add(testingHistoryModel);
//                }
//            } else {
//                Log.e(this.TAG + "--->", "onBackPressed 1");
//                super.onBackPressed();
//            }
//        }
//        if (Share.runAllTest) {
//            Log.e(this.TAG + "----->", "onBackPressed");
//            if (Share.position == 12) {
//                return;
//            }
//            if (Share.position == SENSOR_SENSITIVITY) {
//                Log.e(this.TAG + "-->", "skipTest:: in if check_touch" + this.check_touch);
//                if (this.check_touch) {
//                    this.back_pressed = true;
//                    askResultToUser();
//                    return;
//                }
//                this.check_touch = false;
//                addRunallTestEntry();
//                super.onBackPressed();
//            } else if (Share.position == 8) {
//                if (this.ll_check_clicked) {
//                    this.back_pressed = true;
//                    askResultToUser1();
//                    return;
//                }
//                this.ll_check_clicked = false;
//                addRunallTestEntry();
//                super.onBackPressed();
//            } else if (Share.position != 10) {
//                addRunallTestEntry();
//                super.onBackPressed();
//            } else if (this.ll_check_clicked) {
//                this.back_pressed = true;
//                askResultToUser1();
//            } else {
//                this.ll_check_clicked = false;
//                addRunallTestEntry();
//                super.onBackPressed();
//            }
//        } else if (Share.position == SENSOR_SENSITIVITY) {
//            if (this.check_touch) {
//                askResultToUser();
//                return;
//            }
//            this.check_touch = false;
//            super.onBackPressed();
//        } else if (Share.position == 8) {
//            if (this.ll_check_clicked) {
//                askResultToUser1();
//                return;
//            }
//            this.ll_check_clicked = false;
//            super.onBackPressed();
//        } else if (Share.position == 10) {
//            if (this.ll_check_clicked) {
//                askResultToUser1();
//                return;
//            }
//            this.ll_check_clicked = false;
//            super.onBackPressed();
//        } else if (Share.position != 12) {
//            super.onBackPressed();
//        }
//    }
//
//    private void addRunallTestEntry() {
//        Log.e(this.TAG + "--->", "addRunallTestEntry RunAllTests.runAllHistoryModelArrayList::" + runAllHistoryModelArrayList.size());
//        if (runAllHistoryModelArrayList.size() > 0) {
//            addInHistoryList();
//            Share.runAllHistory.setRunAllHistoryModels(runAllHistoryModelArrayList);
//            Share.runAllHistoryModelArrayList1.add(Share.runAllHistory);
//            this.tinydb.putListObjectRunALlFinal(String.valueOf(Share.time), Share.runAllHistoryModelArrayList1);
//        }
//    }
//
//    private void askResultToUser() {
//        Builder alertDialogBuilder = new Builder(this.activity, R.style.alert_dialog);
//        alertDialogBuilder.setMessage(getResources().getString(R.string.touch_test_passed_or_failed)).setCancelable(false).setPositiveButton(getResources().getString(R.string.touch_passed), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                RunAllTests.this.finish();
//                RunAllTests.this.dialog_showing = false;
//                if (!Share.runAllTest) {
//                    RunAllTests.this.testingHistoryModel.setTest_result(true);
//                    RunAllTests.this.testingHistoryModel.setTime(System.currentTimeMillis());
//                    RunAllTests.testingHistoryModels.add(RunAllTests.this.testingHistoryModel);
//                    Log.e(RunAllTests.this.TAG + "--->", "testingHistoryModels.add  5");
//                }
//                RunAllTests.this.nextTest(true);
//            }
//        }).setNegativeButton(getResources().getString(R.string.touch_failed), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                RunAllTests.this.finish();
//                if (!Share.runAllTest) {
//                    RunAllTests.this.testingHistoryModel.setTest_result(false);
//                    RunAllTests.this.testingHistoryModel.setTime(System.currentTimeMillis());
//                    RunAllTests.testingHistoryModels.add(RunAllTests.this.testingHistoryModel);
//                    Log.e(RunAllTests.this.TAG + "--->", "testingHistoryModels.add  6");
//                }
//                RunAllTests.this.nextTest(false);
//            }
//        });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        if (!this.dialog_showing) {
//            alertDialog.show();
//            this.dialog_showing = true;
//        }
//    }
//
//    private void askResultToUser1() {
//        Builder alertDialogBuilder = new Builder(this.activity, R.style.alert_dialog);
//        alertDialogBuilder.setMessage(getResources().getString(R.string.touch_test_passed_or_failed)).setCancelable(false).setPositiveButton(getResources().getString(R.string.touch_passed), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                RunAllTests.this.finish();
//                RunAllTests.this.dialog_showing = false;
//                if (!Share.runAllTest) {
//                    if (Share.position == 8) {
//                        RunAllTests.this.testingHistoryModel.setFlash(true);
//                    } else if (Share.position == 10) {
//                        RunAllTests.this.testingHistoryModel.setVibrate(true);
//                    }
//                    RunAllTests.this.testingHistoryModel.setTime(System.currentTimeMillis());
//                    RunAllTests.testingHistoryModels.add(RunAllTests.this.testingHistoryModel);
//                    Log.e(RunAllTests.this.TAG + "--->", "testingHistoryModels.add  7");
//                }
//                RunAllTests.this.nextTest(true);
//            }
//        }).setNegativeButton(getResources().getString(R.string.touch_failed), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                RunAllTests.this.finish();
//                if (!Share.runAllTest) {
//                    if (Share.position == 8) {
//                        RunAllTests.this.testingHistoryModel.setFlash(false);
//                    } else if (Share.position == 10) {
//                        RunAllTests.this.testingHistoryModel.setVibrate(false);
//                    }
//                    RunAllTests.this.testingHistoryModel.setTime(System.currentTimeMillis());
//                    RunAllTests.testingHistoryModels.add(RunAllTests.this.testingHistoryModel);
//                    Log.e(RunAllTests.this.TAG + "--->", "testingHistoryModels.add  8");
//                }
//                RunAllTests.this.nextTest(false);
//            }
//        });
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        if (!this.dialog_showing) {
//            alertDialog.show();
//            this.dialog_showing = true;
//        }
//    }
//
//    private void nextTest(boolean result) {
//        Log.e(this.TAG + "--->", "nextTest position::" + Share.position);
//        if (!Share.runAllTest) {
//            return;
//        }
//        if (Share.position == 16) {
//            if (this.check_pressed) {
//                RunAllHistoryModel runAllHistoryModel = new RunAllHistoryModel();
//                runAllHistoryModel.setRun_all_test_name(Share.testingList[Share.position]);
//                runAllHistoryModel.setRun_all_test_image(Share.testingIcon[Share.position]);
//                runAllHistoryModel.setRun_all_test_result(result);
//                runAllHistoryModel.setRun_all_time(Share.time);
//                Log.e(this.TAG + "===>", "Share.time::" + Share.time);
//                runAllHistoryModelArrayList.add(runAllHistoryModel);
//            }
//            this.tinydb.putListObjectRunALl(String.valueOf(Share.time), runAllHistoryModelArrayList);
//            startActivity(new Intent(this.activity, RunAllTestResultActivity.class));
//            finish();
//            return;
//        }
//        runAllHistoryModel = new RunAllHistoryModel();
//        if (Share.position != 12) {
//            if (Share.position != 5) {
//                runAllHistoryModel.setRun_all_test_name(Share.testingList[Share.position]);
//            } else if (this.resultStringHistory.equals(getResources().getString(R.string.record_working))) {
//                runAllHistoryModel.setRun_all_test_name("Record");
//            } else if (this.resultStringHistory.equals(getResources().getString(R.string.ear_piece_working))) {
//                runAllHistoryModel.setRun_all_test_name("Ear Piece");
//            } else {
//                runAllHistoryModel.setRun_all_test_name("Speaker");
//            }
//            runAllHistoryModel.setRun_all_test_image(Share.testingIcon[Share.position]);
//            runAllHistoryModel.setRun_all_test_result(result);
//            runAllHistoryModel.setRun_all_time(Share.time);
//            Log.e(this.TAG + "===>", "Share.time::" + Share.time);
//            if (Share.position == 13 || Share.position == 14 || Share.position == 15) {
//                Log.e(this.TAG + "===>", "nextTest sensor::");
//                if (this.check_pressed) {
//                    runAllHistoryModelArrayList.add(runAllHistoryModel);
//                }
//            } else {
//                Log.e(this.TAG + "-->", "nextTest else zsff");
//                runAllHistoryModelArrayList.add(runAllHistoryModel);
//                if (this.back_pressed) {
//                    addRunallTestEntry();
//                }
//                Log.e(this.TAG + "-->", "nextTest runAllHistoryModelArrayList.size::" + runAllHistoryModelArrayList.size());
//            }
//        }
//        if (Share.position != 5) {
//            Log.e(this.TAG + "--->", "nextTest else back_pressed::" + this.back_pressed);
//            Share.position++;
//            if (this.back_pressed) {
//                finish();
//            } else {
//                callTestActivity();
//            }
//        }
//    }
//
//    protected void onPause() {
//        super.onPause();
//        this.mSensorManager.unregisterListener(this);
//        Log.e(this.TAG + "--->", "testingHistoryModel" + this.testingHistoryModel);
//        Log.e(this.TAG + "--->", "onPause added in model testingHistoryModels::" + testingHistoryModels);
//        if (!Share.runAllTest) {
//            if (Share.position != 12) {
//                this.testingHistoryModel.setPhysical_test_name("null");
//            }
//            Share.sortHistoryArray();
//            this.tinydb.putListObject(Share.TESTING_HISTORY_LIST, testingHistoryModels);
//        }
//    }
//
//    public class ColorAdapter extends PagerAdapter {
//        Context context;
//        LayoutInflater mLayoutInflater;
//
//        ColorAdapter(Context context) {
//            this.context = context;
//            this.mLayoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
//        }
//
//        public int getCount() {
//            return RunAllTests.this.color_array.length;
//        }
//
//        public boolean isViewFromObject(View view, Object object) {
//            return view == ((LinearLayout) object);
//        }
//
//        public Object instantiateItem(ViewGroup container, int position) {
//            View itemView = this.mLayoutInflater.inflate(R.layout.pager_item, container, false);
//            ((ImageView) itemView.findViewById(R.id.iv_color)).setImageResource(RunAllTests.this.color_array[position]);
//            container.addView(itemView);
//            return itemView;
//        }
//    }
//
//    public class DrawingClass {
//        Paint DrawingClassPaint;
//        Path DrawingClassPath;
//
//        public Path getPath() {
//            return this.DrawingClassPath;
//        }
//
//        public void setPath(Path path) {
//            this.DrawingClassPath = path;
//        }
//
//        public Paint getPaint() {
//            return this.DrawingClassPaint;
//        }
//
//        public void setPaint(Paint paint) {
//            this.DrawingClassPaint = paint;
//        }
//    }
//
//    class SketchSheetView extends View {
//        private ArrayList<DrawingClass> DrawingClassArrayList = new ArrayList();
//
//        public SketchSheetView(Context context) {
//            super(context);
//            RunAllTests.this.bitmap = Bitmap.createBitmap(820, 480, Config.ARGB_4444);
//            RunAllTests.this.canvas = new Canvas(RunAllTests.this.bitmap);
//            setBackgroundColor(-1);
//        }
//
//        public boolean onTouchEvent(MotionEvent event) {
//            DrawingClass pathWithPaint = new DrawingClass();
//            RunAllTests.this.canvas.drawPath(RunAllTests.this.path2, RunAllTests.this.paint);
//            if (event.getAction() == 0) {
//                RunAllTests.this.path2.moveTo(event.getX(), event.getY());
//                RunAllTests.this.path2.lineTo(event.getX(), event.getY());
//            } else if (event.getAction() == 2) {
//                RunAllTests.this.path2.lineTo(event.getX(), event.getY());
//                pathWithPaint.setPath(RunAllTests.this.path2);
//                pathWithPaint.setPaint(RunAllTests.this.paint);
//                this.DrawingClassArrayList.add(pathWithPaint);
//            }
//            invalidate();
//            return true;
//        }
//
//        protected void onDraw(Canvas canvas) {
//            super.onDraw(canvas);
//            if (this.DrawingClassArrayList.size() > 0) {
//                canvas.drawPath(((DrawingClass) this.DrawingClassArrayList.get(this.DrawingClassArrayList.size() - 1)).getPath(), ((DrawingClass) this.DrawingClassArrayList.get(this.DrawingClassArrayList.size() - 1)).getPaint());
//            }
//        }
//    }
//}
//
