package com.agiledge.rdaphonemaster.Views;


import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Buttontesting extends Activity implements OnTouchListener {
private static final String TAG = "Touch" ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_buttontestingg);
		
		FrameLayout main = (FrameLayout) findViewById(R.id.main_view);

	}
		public boolean dispatchKeyEvent(KeyEvent event) {
			    int action = event.getAction();
			    int keyCode = event.getKeyCode();
			        switch (keyCode) {
			        case KeyEvent.KEYCODE_VOLUME_UP:
			            if (action == KeyEvent.ACTION_UP) {
			                Toast.makeText(getApplicationContext(), "PRESSED VOLUME UP BUTTON", Toast.LENGTH_SHORT).show();
			            }
			            return true;
			        case KeyEvent.KEYCODE_VOLUME_DOWN:
			            if (action == KeyEvent.ACTION_DOWN) {
			            	Toast.makeText(getApplicationContext(), "PRESSED VOLUME DOWN BUTTON", Toast.LENGTH_SHORT).show(); //TODO
			            }
			            return true;
			  
			      
			       
			        default:
			            return super.dispatchKeyEvent(event);
			        }
			    }
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}



	private void confirm(){
		new SweetAlertDialog(Buttontesting.this, SweetAlertDialog.WARNING_TYPE)
				.setTitleText("Is it Working?")
				.setCancelText("No!")
				.setConfirmText("Yes!")
				.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.dismiss();
						Status status=new Status();
						status.setVolButton("Working");
						finish();



					}
				})
				.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.cancel();
						Status status=new Status();
						status.setVolButton("Not Working");
						finish();
					}
				})
				.show();
	}

	/**
	 * Called when the activity has detected the user's press of the back
	 * key.  The default implementation simply finishes the current activity,
	 * but you can override this to do whatever you want.
	 */
	@Override
	public void onBackPressed() {
		confirm();
	}
	
}
