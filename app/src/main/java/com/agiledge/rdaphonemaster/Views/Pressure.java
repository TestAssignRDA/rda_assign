package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;


public class Pressure extends Activity implements SensorEventListener {
	 TextView tvx;
     SensorManager sensmgr;
     Sensor accsensor;
     float[] sensorvalues;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_presure);
		
		tvx=(TextView)findViewById(R.id.textView00);
		
		sensmgr=(SensorManager)getSystemService(SENSOR_SERVICE);
		accsensor=sensmgr.getDefaultSensor(Sensor.TYPE_PRESSURE);
	}

	@Override
	
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pressure, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
     sensorvalues=event.values;
     float x=sensorvalues[0];
		Status status=new Status();
		status.setPressure("WORKING");
     tvx.setText("x" +x+" hPa");
     
 
    
     
	}
	

	@Override
	protected void onResume() {
    sensmgr.registerListener(this,accsensor, SensorManager.SENSOR_DELAY_NORMAL);

    super.onResume();
	}
	

	@Override
	protected void onPause() {
		sensmgr.unregisterListener(this);
		super.onPause();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
	private void confirm(){
		new android.support.v7.app.AlertDialog.Builder(this)
				.setTitle("Pressure")
				.setMessage("Did It Work?")
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {

					}})
				.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {

					}}).show();
	}
}
