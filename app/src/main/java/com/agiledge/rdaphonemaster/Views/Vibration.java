package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;


public class Vibration extends Activity {
Button b1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vibrate);
		b1=(Button)findViewById(R.id.but101);
		b1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Vibrator v1 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				v1.vibrate(400);
				Toast.makeText(getApplicationContext(), "Your Vibration Working Very Well", Toast.LENGTH_SHORT).show();
				Status status=new Status();
				status.setVibration("Working");
			}
		});
	}
}
