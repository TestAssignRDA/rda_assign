package com.agiledge.rdaphonemaster.Views;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.RelativeLayout;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Display extends AppCompatActivity {
	int [] a;
	RelativeLayout r;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_displayy);
		r=(RelativeLayout) findViewById(R.id.rr);
		a=new int[10];
		a[0]= Color.parseColor("#f4c2c6");
		a[1]= Color.parseColor("#c7afce");
		a[2]= Color.parseColor("#fbcab3");
		a[3]= Color.parseColor("#ffe8f4");
		a[4]= Color.parseColor("#525252");
		a[5]= Color.parseColor("#bcdbbe");
		a[6]= Color.parseColor("#1ca589");
		a[7]= Color.parseColor("#c1073f");
		a[8]= Color.parseColor("#edcdc3");
		a[9]= Color.parseColor("#ecffef");

		
		}
		public boolean onTouchEvent(android.view.MotionEvent event){
		int x=(int) (Math.random()*10);
		r.setBackgroundColor(a[x]);
			return false;
			}

	private void confirm(){
		new SweetAlertDialog(Display.this, SweetAlertDialog.WARNING_TYPE)
				.setTitleText("Is it Working?")
				.setCancelText("No!")
				.setConfirmText("Yes!")
				.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.dismiss();
						Status status=new Status();
						status.setDisplay("Working");
						finish();


					}
				})
				.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.cancel();
						Status status=new Status();
						status.setDisplay("Not Working");
						finish();
					}
				})
				.show();
	}

	/**
	 * Called when the activity has detected the user's press of the back
	 * key.  The default implementation simply finishes the current activity,
	 * but you can override this to do whatever you want.
	 */
	@Override
	public void onBackPressed() {
		Log.d("","Back button pressed");
		super.onBackPressed();
		confirm();
	}
}
