package com.agiledge.rdaphonemaster.Views.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.agiledge.rdaphonemaster.R;


/**
 * Created by pateelhs_agile on 30/09/18.
 */

public class DeviceScanFragment extends Fragment {

    private Button mScanButton;
    private ProgressBar mScanProgressBar;
    private int progressStatus = 0;
    private Handler mHandler = new Handler();

    public DeviceScanFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_device_scan, container, false);

        mScanButton = (Button)rootView.findViewById(R.id.scan_button);
        mScanProgressBar = (ProgressBar)rootView.findViewById(R.id.scan_progressbar);

        mScanProgressBar.setVisibility(View.VISIBLE);

        mScanButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
                new Thread(new Runnable() {
                    public void run() {
                        while (progressStatus < 0) {
                            progressStatus++;
                            mHandler.post(new Runnable() {
                                public void run() {
                                    mScanProgressBar.setProgress(progressStatus);
                                }
                            });
                        }
                    }
                }).start();

            }
        });

        return rootView;
    }
}
