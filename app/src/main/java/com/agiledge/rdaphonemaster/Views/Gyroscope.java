package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Gyroscope extends Activity implements SensorEventListener {
    TextView tvx,tvy,tvz;
    SensorManager sensmgr;
    Sensor gyrosensor;
    float[] sensorvalues;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gyroscope1);
		
		tvx=(TextView)findViewById(R.id.textView8);
		tvy=(TextView)findViewById(R.id.textView9);
		tvz=(TextView)findViewById(R.id.textView10);
		
		sensmgr=(SensorManager)getSystemService(SENSOR_SERVICE);
		gyrosensor=sensmgr.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
	}

	@Override
	
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gyroscope, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
   sensorvalues=event.values;
   float x=sensorvalues[0];
   float y=sensorvalues[1];
   float z=sensorvalues[2];	
   tvx.setText("x "+x+" rad/sec");
   tvy.setText("y "+y+" rad/sec");
   tvz.setText("z "+z+" rad/sec");
   
   
	}
	

	@Override
	protected void onResume() {
  sensmgr.registerListener(this,gyrosensor, SensorManager.SENSOR_DELAY_NORMAL);

  super.onResume();
	}
	

	@Override
	protected void onPause() {
		sensmgr.unregisterListener(this);
		super.onPause();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	private void confirm(){
		new SweetAlertDialog(Gyroscope.this, SweetAlertDialog.WARNING_TYPE)
				.setTitleText("Is it Working?")
				.setCancelText("No!")
				.setConfirmText("Yes!")
				.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.dismiss();
						Status status=new Status();
						status.setGyro("Working");
						finish();


					}
				})
				.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.cancel();
						Status status=new Status();
						status.setGyro("Not Working");
						finish();
					}
				})
				.show();
	}
	/**
	 * Called when the activity has detected the user's press of the back
	 * key.  The default implementation simply finishes the current activity,
	 * but you can override this to do whatever you want.
	 */
	@Override
	public void onBackPressed() {
		confirm();
	}
}
