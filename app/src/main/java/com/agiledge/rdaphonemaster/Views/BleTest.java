package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;
import com.agiledge.rdaphonemaster.Views.Fragments.DeviceScanFragment;


/**
 * Created by pateelhs_agile on 28/09/18.
 */

public class BleTest extends Activity {
    // Log
    private static final String TAG = "MainActivity";
    // Request codes
    private static final int REQUEST_ENABLE_BT = 1;
    private static BluetoothManager mBluetoothManager;
    // BLE
    private boolean mBleSupported = true;
    private BluetoothAdapter mBluetoothAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ble);

        // Determine whether BLE is supported on the device
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Bluetooth Not Supported", Toast.LENGTH_LONG).show();
            mBleSupported = false;
            Log.e(TAG, "BLE is not supported on device");
        }

        // Initializes a Bluetooth adapter
        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Status status = new Status();
            status.setBluetooth("WORKING");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.container, new DeviceScanFragment())
                    .commit();
        }
    }




}

