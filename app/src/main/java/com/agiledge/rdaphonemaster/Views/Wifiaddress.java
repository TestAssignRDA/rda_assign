package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.R;


public class Wifiaddress extends Activity {
TextView t,u,v;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wifiaddress);
		Status status=new Status();
		
		t=(TextView)findViewById(R.id.textView32);
		
		v=(TextView)findViewById(R.id.textView30);
		WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		String macAddress = wInfo.getMacAddress();
		String n= wInfo.LINK_SPEED_UNITS;
		String y=wInfo.getSSID();
		if (macAddress == null)
		{
			status.setWifi("Not Working");
	        macAddress = "Device don't have mac address or wi-fi is disabled";
	      }
		t.setText("mac address "+macAddress);

		status.setWifi("Working");
		
		v.setText("ssid "+y);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.wifiaddress, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
