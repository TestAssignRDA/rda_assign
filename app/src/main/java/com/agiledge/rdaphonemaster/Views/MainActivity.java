package com.agiledge.rdaphonemaster.Views;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.agiledge.rdaphonemaster.Commons.TinyDB;
import com.agiledge.rdaphonemaster.Model.Status;
import com.agiledge.rdaphonemaster.Model.TestingHistoryModel;
import com.agiledge.rdaphonemaster.R;
import com.agiledge.rdaphonemaster.Utils.PermissionManager;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class MainActivity extends Activity implements OnItemClickListener {
    private static final int CAMERA_REQUEST = 1888;
    public static boolean FRONT_CAMERA = false;
    Intent intent;
    //AutoCompleteTextView act;
    PermissionManager permissionManager;
    TinyDB tinydb;
    Status status;
    private static final int PHOTO_REQUEST_CODE = 102;
    private String path;
    ListView lv;
    //Spinner spn;
    String items[] = {"Vibration Test", "Check Version Info", "SIM Card", "Proximity Sensor",
            "Flash Light", "Touch Sensor", "Display", "Light Sensor", "Pressure Sensor"
            , "Phone Buttons", "Speaker Test", "Wi-Fi Address", "Bluetooth Address", "Gravity sensor", "Magnetic Sensor", "Headphone",
            "Gyroscope", "GPS Location", "Battery Indicator", "Accelarometer", "Mic Testing"};
    private int CAMERA_CODE = 432;
    private boolean EAR_PHONE = false;
    private boolean IS_EAR_PIECE = false;
    private boolean IS_RECORDING = false;
    private boolean IS_SPEAKER = false;
    private Uri capturedImageUri = null;
    private boolean SPEAKER = false;
    private int STORAGE_PERMISSION_CODE = 23;
    private String TAG = "RunAllTests";
    private File f;
    private List<String> listPermissionsNeeded = new ArrayList();
    private boolean isFRONT_CAMERA = false;

    /**
     * Checks if the device is rooted.
     *
     * @return <code>true</code> if the device is rooted, <code>false</code> otherwise.
     */
    public static boolean isRooted() {

        // get from build info
        String buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
            return true;
        }

        // check if /system/app/Superuser.apk is present
        try {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
            // ignore
        }

        // try executing commands
        return canExecuteCommand("/system/xbin/which su")
                || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su");
    }

//    private static boolean isAirplaneModeOn(Context context) {
//       return System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
//   }

    // executes a command on the system
    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        checkAndRequestPermissions();
        lv = (ListView) findViewById(R.id.listView1);
        ArrayAdapter<String> adapt = new ArrayAdapter<String>(this, R.layout.extraa, items);
        lv.setAdapter(adapt);
        status = new Status();
        lv.setOnItemClickListener(this);
        permissionManager=new PermissionManager();
        status.setSim("Not Tested");
        status.setSpeaker("Not Tested");
        status.setMic("Not Tested");
        status.setMagentic("Not Tested");
        status.setLightSensor("Not Tested");
        status.setHeadPhone("Not Tested");
        status.setBluetooth("Not Tested");
        status.setGyro("Not Tested");
        status.setFlash("Not Tested");
        status.setVibration("Not Tested");
        status.setWifi("Not Tested");
        status.setAccelarometer("Not Tested");





        if (isRooted()) {
            status.setRoot("Rooted");

        } else {
            status.setRoot("Rooted");

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        // TODO Auto-generated method stub
        switch (pos) {
            case 0:
                //not working
                intent = new Intent(this, Vibration.class);
                startActivity(intent);
                break;
            case 2:
                sim();
                break;
            case 1:
                //not whole info
                intent = new Intent(getApplicationContext(), Systeminfo.class);
                startActivity(intent);
                break;
            case 3:

                intent = new Intent(getApplicationContext(), ProximitySensor.class);
                startActivity(intent);
                break;

            case 4:
                //not working
                intent = new Intent(getApplicationContext(), Flash.class);
                startActivity(intent);
                break;

            case 5:

                intent = new Intent(getApplicationContext(), TouchSensor.class);
                startActivity(intent);
                break;
            case 6:
                //display multiple lighting
                intent = new Intent(getApplicationContext(), Display.class);
                startActivity(intent);
                break;
            case 7:
                //light sensor display of dac values
                intent = new Intent(getApplicationContext(), Lightsensor.class);
                startActivity(intent);
                break;
            case 8:

                intent = new Intent(getApplicationContext(), Pressure.class);
                startActivity(intent);
                break;

            case 9:
                //wrong the volume buttons,home,menu etc has to be checked here
                intent = new Intent(getApplicationContext(), Buttontesting.class);
                startActivity(intent);
                break;
            case 10:
                //wrong should have done that while taking calling position we here our voice
                intent = new Intent(getApplicationContext(), Mictesting.class);
                startActivity(intent);
                break;
            case 11:

                intent = new Intent(getApplicationContext(), Wifiaddress.class);
                startActivity(intent);
                break;
            case 12:

                intent = new Intent(getApplicationContext(), BleTest.class);
                startActivity(intent);
                break;
            case 13:

                intent = new Intent(getApplicationContext(), Gravitysensor.class);
                startActivity(intent);
                break;

            case 14:
                intent = new Intent(getApplicationContext(), Magneticsensor.class);
                startActivity(intent);
                break;
            case 15:
                //not working
                intent = new Intent(getApplicationContext(), Headphone.class);
                startActivity(intent);
                break;
            case 16:
                intent = new Intent(getApplicationContext(), Gyroscope.class);
                startActivity(intent);
                break;
            case 17:
                //test again
                intent = new Intent(getApplicationContext(), Gpsloc.class);
                startActivity(intent);
                break;
            case 18:
                intent = new Intent(getApplicationContext(), Batteryindicator.class);
                startActivity(intent);
                break;
            case 19:
                intent = new Intent(getApplicationContext(), Accelarometer.class);
                startActivity(intent);
                break;
            case 20:
                intent = new Intent(getApplicationContext(), MicTest.class);
                startActivity(intent);
                break;

        }


    }

    private boolean checkAndRequestPermissions() {
        this.listPermissionsNeeded.clear();
        int read_storage = ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE");
        int storage = ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE");
        int camera = ContextCompat.checkSelfPermission(this, "android.permission.CAMERA");
        int record_audio = ContextCompat.checkSelfPermission(this, "android.permission.RECORD_AUDIO");
        if (read_storage != 0) {
            this.listPermissionsNeeded.add("android.permission.READ_EXTERNAL_STORAGE");
        }
        if (storage != 0) {
            this.listPermissionsNeeded.add("android.permission.WRITE_EXTERNAL_STORAGE");
        }
        if (camera != 0) {
            this.listPermissionsNeeded.add("android.permission.CAMERA");
        }
        if (record_audio != 0) {
            this.listPermissionsNeeded.add("android.permission.RECORD_AUDIO");
        }
        Log.e("TAG", "listPermissionsNeeded===>" + this.listPermissionsNeeded);
        if (this.listPermissionsNeeded.isEmpty()) {
            return true;
        }
        return false;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(this.TAG, "resultCode :" + resultCode);
        if (requestCode == this.STORAGE_PERMISSION_CODE && resultCode == -1) {
            //startRecording();
        }
        if (requestCode == this.CAMERA_CODE) {
            Log.e(this.TAG + "--->", "CAMERA_CODE");
            if (resultCode == -1) {
                Log.e(this.TAG + "--->", "RESULT_OK");
                Log.e(this.TAG + "--->", "capturedImageUri::" + this.capturedImageUri);
                if (this.capturedImageUri != null) {
                    File file = new File(this.capturedImageUri.getPath());
                    Log.e(this.TAG + "--->", "file:::" + file);
                    if (file.exists()) {
                        Log.e(this.TAG + "--->", "result success");
                        if (FRONT_CAMERA) {
                            this.isFRONT_CAMERA = true;

                            FRONT_CAMERA = false;
                            return;
                        }
                        this.isFRONT_CAMERA = false;
                        FRONT_CAMERA = true;
                        return;
                    }
                    TestingHistoryModel testingHistoryModel = new TestingHistoryModel();
                    if (FRONT_CAMERA) {
                        this.isFRONT_CAMERA = true;

                        return;
                    }
                    this.isFRONT_CAMERA = false;

                    FRONT_CAMERA = false;
                }
            }
        }
        else if (requestCode == PHOTO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Bitmap source = BitmapFactory.decodeFile(path, provideCompressionBitmapFactoryOptions());

        }
    }

    private void sim() {
        TelephonyManager telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                Toast.makeText(getApplicationContext(), "Sim State Absent", Toast.LENGTH_SHORT).show();
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                Toast.makeText(getApplicationContext(), "Sim State Network Locked", Toast.LENGTH_SHORT).show();// do something
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                Toast.makeText(getApplicationContext(), "Sim State Pin Required", Toast.LENGTH_SHORT).show(); // do something
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                Toast.makeText(getApplicationContext(), "Sim State Puk Required", Toast.LENGTH_SHORT).show();// do something
                break;
            case TelephonyManager.SIM_STATE_READY:
                Toast.makeText(getApplicationContext(), "Sim State Ready", Toast.LENGTH_SHORT).show();// do something

                status.setSim("WORKING");
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                Toast.makeText(getApplicationContext(), "Sim State Unknown", Toast.LENGTH_SHORT).show(); // do something

                status.setSim("NOT WORKING");
                break;
        }
    }

    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.magneticsensor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_share) {
            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Screen Broken?")
                    .setCancelText("No!")
                    .setConfirmText("Yes!")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Capture a Image of phone screen from your front camera standing infront of a Mirror",
                                    Toast.LENGTH_LONG).show();
                            if (permissionManager.userHasPermission(MainActivity.this)) {
                                takePicture();
                            } else {
                                permissionManager.requestPermission(MainActivity.this);
                            }


                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                            sendEmail(null);
                        }
                    })
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }


    public File getAlbumDir() {

        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES
                ),
                "BAC/"
        );
        // Create directories if needed
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        return storageDir;
    }

    private File createImageFile() throws IOException {
        // Create an image file name

        String imageFileName = getAlbumDir().toString() + "/image.jpg";
        File image = new File(imageFileName);
        return image;
    }

    private void sendEmail(Uri screenshot) {
        String email = getString(R.string.report_issues_link);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "\nnAaccelerometer: " + status.getAccelarometer() +
                "\nTouch: " + status.getDisplay() +
                "\nBluetooth: " + status.getBluetooth() +
                "\nMagnet:" + status.getMagentic() +
                "\nMic: " + status.getMic() +
                "\nSpeaker: " + status.getSpeaker() +
                "\nSim: " + status.getSim()+
                "\nButtons: " + status.getVolButton() +
                "\nWifi: " + status.getWifi()+
                "\nHeadPhone: " + status.getHeadPhone() +
                "\nCamera: " + status.getCamera());
        if (screenshot != null) {
            intent.putExtra(Intent.EXTRA_STREAM, screenshot);
        }
        startActivity(Intent.createChooser(intent, "Send Device status..."));
    }

    private void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            Uri photoURI = null;
            try {
                File photoFile = createImageFileWith();
                path = photoFile.getAbsolutePath();
                photoURI = FileProvider.getUriForFile(MainActivity.this,
                        getString(R.string.file_provider_authority),
                        photoFile);
                sendEmail(photoURI);
                status.setCamera("Working");

            } catch (IOException ex) {
                Log.e("TakePicture", ex.getMessage());
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                takePictureIntent.setClipData(ClipData.newRawUri("", photoURI));
                takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
            startActivityForResult(takePictureIntent, PHOTO_REQUEST_CODE);
        }
    }

    private static BitmapFactory.Options provideCompressionBitmapFactoryOptions() {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = false;
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        return opt;
    }

    public File createImageFileWith() throws IOException {
        final String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        final String imageFileName = "JPEG_" + timestamp;
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "pics");
        storageDir.mkdirs();
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }
    }

